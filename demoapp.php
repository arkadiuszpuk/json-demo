<?php

Class Flattener
{
  private $jsonData;
  private $flatArray = [];

  public function __construct(string $jsonData)
  {
      $this->jsonData = $jsonData;
  }

  public function flatten(): array
  {
    $this->flatArray = [];
    $this->processArrayData($this->getJsonDataAsArray());
    return $this->flatArray;
  }

  private function getJsonDataAsArray(): array
  {
    return json_decode($this->jsonData);
  }

  private function processArrayData(array $data)
  {
    foreach ($data as $element) {
      if (is_array($element)) {
        $this->processArrayData($element);
      } else {
        $this->flatArray[] = $element;
      }
    }
  }
}

$jsonArray = '["Paris", ["Berlin", "London", ["Ontario"]],"Florida", ["California", "Amsterdam", ["Palma","Barcelona", ["Glasgow", ["Sydney"]]]],"Budapest"]';
print_r((new Flattener($jsonArray))->flatten());
